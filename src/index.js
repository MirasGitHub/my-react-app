import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import "./slider.css";
// import App from "./App";
import reportWebVitals from "./reportWebVitals";

let heading = React.createElement(
	"h1",
	{ style: { color: "#999", fontSize: "19px" } },
	"Solar system planets:"
);

let mercury = "Mercury";
let venus = "Venus";
let earth = "Earth";
let mars = "Mars";
let jupiter = "Jupiter";
let saturn = "Saturn";
let uranus = "Uranus";
let neptune = "Neptune";

let list = (
	<ul className="planets-list">
		<li>{mercury}</li>
		<li>{venus}</li>
		<li>{earth}</li>
		<li>{mars}</li>
		<li>{jupiter}</li>
		<li>{saturn}</li>
		<li>{uranus}</li>
		<li>{neptune}</li>
	</ul>
);

const slider = (
	<label className="switch" htmlFor="checkbox">
		<input type="checkbox" id="checkbox" />
		<div className="slider round" onClick={changeTheme}></div>
	</label>
);

function changeTheme() {
	const body = document.body;
	body.classList.toggle("dark");
}

const App = () => {
	return (
		<div>
			{slider}
			{heading}
			{list}
		</div>
	);
};

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
	<React.StrictMode>
		<App />
	</React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
